package com.example.createoverlay;

import androidx.appcompat.app.AppCompatActivity;

import android.app.Activity;
import android.content.ComponentName;
import android.content.Intent;
import android.content.ServiceConnection;
import android.graphics.Point;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;

import java.util.Vector;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = "MainActivity";
    public static final int CODE_DRAW_OVER_OTHER_APP_PERMISSION = 2084;
    boolean mBounded;
    FloatingService mServer;
    private Button startButton;
   @Override
   protected void onStart()
   {
       super.onStart();
       Intent intent = new Intent(this,FloatingService.class);
       startService(intent);
       bindService(intent,mConnection,BIND_AUTO_CREATE);
   }
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        startButton=(Button)findViewById(R.id.start);
        startButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v)
            {
                if(isLayoutOverlayPermissionGranted(MainActivity.this))
                {
                    finish();//close main activity
//                    Vector<BlurData> blurdata=new Vector<BlurData>();
//                    blurdata.add(new BlurData(0,0,60,60));
//                    blurdata.add(new BlurData(100,20,40,30));
//                    mServer.blur(blurdata);
                    Vector<Point> points=new Vector<Point>();
                    points.add(new Point(100,100));
                    points.add(new Point(0,0));
                    mServer.blur(points);
                }
                else
                {
                    grantLayoutOverlayPermission(MainActivity.this);
                }
            }
        });
    }//onCreate
    private boolean isLayoutOverlayPermissionGranted(Activity activity)
    {
        Log.v(TAG,"Granting Layout Overlay Permission..");
        if(Build.VERSION.SDK_INT>=23 && !Settings.canDrawOverlays(activity))
        {
            Log.v(TAG,"Permission is denied");
            return false;
        }
        else
        {
            Log.v(TAG,"Permission is granted");
            return true;
        }
    }//isLayoutOverlayPermissionGranted
    private void grantLayoutOverlayPermission(Activity activity)
    {
        if(Build.VERSION.SDK_INT>=23 &&!Settings.canDrawOverlays(activity))
        {
            Intent intent=new Intent(Settings.ACTION_MANAGE_OVERLAY_PERMISSION,
                    Uri.parse("package:" + activity.getPackageName()));
            activity.startActivityForResult(intent,CODE_DRAW_OVER_OTHER_APP_PERMISSION);
        }
    }
    ServiceConnection mConnection = new ServiceConnection()
    {
        @Override
        public void onServiceDisconnected(ComponentName name)
        {
            mBounded = false;
            mServer = null;
        }
        @Override
        public void onServiceConnected(ComponentName name, IBinder service)
        {
            mBounded = true;
            FloatingService.LocalBinder mLocalBinder = (FloatingService.LocalBinder)service;
            mServer = mLocalBinder.getServerInstance();
        }
    };
}
